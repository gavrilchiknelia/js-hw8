let focusInput = document.getElementById('input');
let wrap;
 function blu() {
     let newSpan;
     let btn;
    if (isNaN(this.value) || this.value <= 0 || this.value > 100000) {
        this.style.border = '3px solid red';
        document.getElementById('error').innerHTML = 'Please enter correct price';
        this.value = '';
    } else if (this.value === '') {
        this.style.border = '';
    } else {
        this.style.border = '';
        wrap = document.createElement('div');
        wrap.className = "current-price";
        document.body.insertBefore(wrap, document.body.firstChild);

        newSpan = document.createElement('span');
        newSpan.innerHTML = `Текущая цена: ${this.value}`;
        wrap.appendChild(newSpan);

        btn = document.createElement('button');
        btn.innerHTML = 'x';
        btn.className = 'btn-border';
        wrap.appendChild(btn);
        this.value = '';
    }
    if (btn) {
        btn.onclick = click;
    }
}
function foc() {
    if (this.style.border === '3px solid red') {
        this.style.border = '3px solid green';
        document.getElementById('error').innerHTML = "";
    } else {
        this.style.border = '3px solid green';
    }
}
function click () {
    let el = this.parentNode;
    el.parentNode.removeChild(el);
}

focusInput.onfocus = foc;
focusInput.onblur = blu;